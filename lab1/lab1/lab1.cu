#include "lab1.h"
#include <stdio.h>
#include <stdlib.h>
#include <GLFW/glfw3.h>
static const unsigned W = 600;
static const unsigned H = 600;
static const unsigned NFRAME = 240;
#define DIM 600
__device__ float T = 0.0f;
__device__ bool downmode=false;
struct Lab1VideoGenerator::Impl {
	int t = 0;
};

Lab1VideoGenerator::Lab1VideoGenerator(): impl(new Impl) {

}

Lab1VideoGenerator::~Lab1VideoGenerator() {}

void Lab1VideoGenerator::get_info(Lab1VideoInfo &info) {
	info.w = W;
	info.h = H;
	info.n_frame = NFRAME;
	// fps = 24/1 = 24
	info.fps_n = 24;
	info.fps_d = 1;
};

__device__ void pixel(uint8_t *yuv, int x, int y, int r, int g, int b) {
	// y
	yuv[y * W + x] = 0.299 * r + 0.587 * g + 0.114 * b;

	if (y % 2 == 0 && x % 2 == 0) {
		int offset = y * W / 4 + x / 2;
		// u
		yuv[W * H + offset] = -0.169 * r - 0.331 * g + 0.500 * b + 128;
		// v
		yuv[W * H * 5 / 4 + offset] = 0.500 * r - 0.419 * g - 0.081 * b + 128;
	}
}


struct cuComplex {
	float r;
	float i;

	__device__ cuComplex(float a, float b) : r(a), i(b) {
	}

	__device__ float magnitude2(void) {
		return r * r + i * i;
	}

	__device__ cuComplex operator*(const cuComplex& a) {
		return cuComplex(r * a.r - i * a.i, i * a.r + r * a.i);
	}

	__device__ cuComplex operator+(const cuComplex& a) {
		return cuComplex(r + a.r, i + a.i);
	}
};

__device__ int julia(int x, int y) {
	const float scale = 1.5;
	int dim_2 = DIM / 2;
	float jx = scale * (float)(dim_2 - x) / dim_2;
	float jy = scale * (float)(dim_2 - y) / dim_2;

	cuComplex c(-0.7+T, 0.146+T);
	cuComplex a(jx, jy);

	for (int i = 0; i < 200; i++) {
		a = a * a + c;

		if (a.magnitude2() > 1000) {
			return 0;
		}
	}
	return 1;
}

__global__ void kernel(uint8_t *yuv) {
	int x = blockIdx.x * blockDim.x + threadIdx.x;
	int y = blockIdx.y * blockDim.y + threadIdx.y;;

	int offset = x + y * gridDim.x;
	int juliavalue = julia(x, y);
	//unsigned char *ptr;
	//ptr[offset * 4 + 0] = 127 * juliavalue;
	//ptr[offset * 4 + 1] = 205 * juliavalue;
	//ptr[offset * 4 + 2] = 100;
	//ptr[offset * 4 + 3] = 200;
	pixel(yuv, x, y, 207 * juliavalue+T*200, 205 * juliavalue + T * 100, 100 + T*(100) );

	
	if (downmode==false) {
		T += 0.000001f;
		if(T>=0.6)
		downmode = true;
	}
	if (downmode == true)
	{
		T -= 0.000001f;
		if (T <= 0)
			downmode = false;
	}
		
}

struct CPUBitmap {
	unsigned char   *pixels;
	int     x, y;
	void    *dataBlock;

	CPUBitmap(int width, int height, void *d = NULL) {
		pixels = new unsigned char[width * height * 4];
		x = width;
		y = height;
		dataBlock = d;
	}

	~CPUBitmap() {
		delete[] pixels;
	}

	unsigned char* get_ptr(void) const { return pixels; }
	long image_size(void) const { return x * y * 4; }

	void display_and_exit() {
		CPUBitmap**   bitmap = get_bitmap_ptr();
		*bitmap = this;
		GLFWwindow* window;

		if (!glfwInit()) {
			printf("Failed on GLFW init\n");
			exit(EXIT_FAILURE);
		}

		window = glfwCreateWindow(x, y, "Julia Set", NULL, NULL);
		if (!window) {
			glfwTerminate();
			printf("Failed to create window\n");
			exit(EXIT_FAILURE);
		}

		glfwMakeContextCurrent(window);
		glfwSetKeyCallback(window, key_callback);

		while (!glfwWindowShouldClose(window)) {
			CPUBitmap*   bitmap = *(get_bitmap_ptr());
			glClearColor(0.0, 0.0, 1.0, 0.0);
			glClear(GL_COLOR_BUFFER_BIT);
			glDrawPixels(bitmap->x, bitmap->y, GL_RGBA, GL_UNSIGNED_BYTE, bitmap->pixels);
			glfwSwapBuffers(window);
			glfwPollEvents();

		}
		glfwTerminate();
	}

	// static method used for glut callbacks
	static CPUBitmap** get_bitmap_ptr(void) {
		static CPUBitmap   *gBitmap;
		return &gBitmap;
	}

	static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
	{
		if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS) {
			glfwSetWindowShouldClose(window, GL_TRUE);
			glfwTerminate();
			exit(EXIT_SUCCESS);
		}
	}
};
void handleError(cudaError_t error, int lineNo) {
	if (error != cudaSuccess) {
		printf("Error: %s\n in file %s at line no %d\n",
			cudaGetErrorString(error), __FILE__, __LINE__);
		exit(EXIT_FAILURE);
	}
}
void Lab1VideoGenerator::Generate(uint8_t *yuv) {

	//cudaMemset(yuv, (impl->t) * 255 / NFRAME, W*H);
	//cudaMemset(yuv + W*H, 128, W*H / 2);
	CPUBitmap bitmap(DIM, DIM);
	unsigned char *dev_bitmap;
	dim3 grid(DIM, DIM);
	kernel << <grid, 1 >> > (yuv);
	
	++(impl->t);
	
	
}