#include "counting.h"
#include <cstdio>
#include <cassert>
#include <thrust/scan.h>
#include <thrust/transform.h>
#include <thrust/functional.h>
#include <thrust/device_ptr.h>
#include <thrust/execution_policy.h>
#include <thrust/find.h>
#include <thrust/inner_product.h>
#include <device_launch_parameters.h>

__device__ __host__ int CeilDiv(int a, int b) { return (a-1)/b + 1; }
__device__ __host__ int CeilAlign(int a, int b) { return CeilDiv(a, b) * b; }
template<typename T>
struct meStruct {
	__host__ __device__
		bool operator()(const T &lhs, const T &rhs)const { return rhs != '\n'; }
};
__global__
void Countword(const char *text, int *pos, int text_size) {
	const int y = blockIdx.y*blockDim.y + threadIdx.y;
	const int x = blockIdx.x*blockDim.x + threadIdx.x;
	int a = y * 7200 + x;
	int id = a;
	int count = 0;
	if (a < text_size)
	{
		while (text[id] != '\n' && id >=0)
		{
			id--;
			count++;
		}
		pos[a] = count;
		
	}
}
void CountPosition1(const char *text, int *pos, int text_size)
{

	thrust::device_ptr<char> textptr((char *)text);
	thrust::device_ptr<int> textpos((int*)pos);
	thrust::fill(textpos, textpos + text_size, 1);
	thrust::device_ptr<char> Notenp = thrust::find(textptr, textptr + text_size, '\n');
	meStruct<char> binary_pred;
	thrust::exclusive_scan_by_key(Notenp, textptr + text_size, textpos + (Notenp - textptr), textpos + (Notenp - textptr), 0, binary_pred);
	thrust::inclusive_scan_by_key(textptr, Notenp, textpos, textpos, binary_pred);
	//std:: cout << text_size;
}

void CountPosition2(const char *text, int *pos, int text_size)
{
	dim3 block(240,240);
	dim3 thread(30,30);
	//std::cout << "test";
	Countword << <block, thread >> >(text, pos, text_size);
}
